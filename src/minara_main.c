/*
    minara - a programmable graphics program editor
    Copyright (C) 2004  Rob Myers rob@robmyers.org

    This file is part of minara.

    minara is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    minara is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with minara.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
  Introduce making windows to Guile...
*/

/*-----------------------------------------------------------------------------
  Includes
  ---------------------------------------------------------------------------*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "libguile.h"

#ifdef __APPLE__
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>
#include <GLUT/glut.h>
#else
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>
#endif

#include "minara_events.h"
#include "minara_guile.h"
#include "minara_rendering.h"
#include "minara_cache.h"
#include "minara_window.h"
#include "minara_menu.h"

/*-----------------------------------------------------------------------------
  Main Program Entry Point
  
  Starts the window system, starts the Lisp system, and loads the Lisp
  bootstrap code.
  So we start GLUT and Guile, then load minara-bootstrap.scm .
  ---------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------
  Constants
  ---------------------------------------------------------------------------*/

#define BOOTSTRAP_FILE "minara-bootstrap.scm"


/*-----------------------------------------------------------------------------
  Globals
  ---------------------------------------------------------------------------*/


/*-----------------------------------------------------------------------------
  Functions
  ---------------------------------------------------------------------------*/

//Program lifecycle

/**
   The procedure called by guile as our real main.
   Initialises SDL and guile, runs the main event loop,
   shuts down SDL and guile, then exits.
*/

void
real_main ()
{
  //Register all the Guile extensions
  guile_startup ();
  rendering_startup ();
  cache_startup ();
  window_startup ();
  menu_startup ();
  events_startup ();
  //Bootstrap the Guile code (libraries, tools, etc.)
  // Here so all the C extensions are loaded first and GLUT is initialised
  scm_c_primitive_load (MINARA_LISP_DIR "/" BOOTSTRAP_FILE);
  //Main event loop
  glutMainLoop ();
  //We quit here
  exit (0);
}

/**
   Our main. Just copies arc/v and calls Guile with our RealMain .
*/

int
main (int argc, char **argv)
{
  //Init GLUT
  glutInit (&argc, argv);
  //never returns
  scm_boot_guile (argc, argv, real_main, NULL);
  //Keep the compiler happy...
  return 0;
}
